package com.life.serializable.utils;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 14/02/2018.
 */

public class ReaderThread extends Thread {
    private final static String TAG = "ReaderThread";

    List<Byte> readBuffer;
    InputStream mInputStream;

    ReaderThreadCallbacks readerThreadCallbacks;

    public void setReaderThreadCallbacks(ReaderThreadCallbacks readerThreadCallbacks) {
        this.readerThreadCallbacks = readerThreadCallbacks;
    }

    public ReaderThread(InputStream mInputStream) {
        this.mInputStream = mInputStream;
        readBuffer = new ArrayList<>();
    }

    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

        while (true) {
            try {

                int r = mInputStream.read();

                if (r > 0) {
                    readBuffer.add((byte) r);
                } else if (!readBuffer.isEmpty()) {

                    byte[] m = new byte[readBuffer.size()];
                    int i = 0;
                    for (Byte b : readBuffer.toArray(new Byte[0])) {
                        m[i++] = b.byteValue();
                    }
                    readBuffer.clear();

                    if (readerThreadCallbacks != null)
                        readerThreadCallbacks.onDataRead(m);
                }

            } catch (IOException e) {
                Log.e(TAG, "IOException", e);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }
}