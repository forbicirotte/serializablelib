package com.life.serializable.utils;

import android.bluetooth.BluetoothGatt;

import java.util.UUID;

import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;

/**
 * Created by User on 13/02/2018.
 */

public class Consts {

    public enum Role {SERVER, CLIENT}

    public static int READ_TIMEOUT = 200;

    //Send next chunck
    public static final byte ACK = 0x01;
    //Something went wrong
    public static final byte NACK = 0x02;

    public static final UUID SERVICE_UUID = UUID.fromString("e7810a71-73ae-499d-8c15-faa9aef0c3f2");
    public static final UUID CHARACTERISTIC_TX_UUID = UUID.fromString("bef8d6c9-9c21-4c9e-b632-bd58c1009f9f");
    public static final UUID CHARACTERISTIC_RX_UUID = UUID.fromString("bef8d6c9-9c21-4c9e-b632-bd58c1009f9a");

    //Needed by iOS device for notifications
    public static final UUID DESCRIPTOR_CONFIG_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    public static final int MTU_MINIMUM_SIZE = 20;

    public static String getGattStatusString(int status) {

        String gattResult = "UNKNOWN";

        switch (status) {
            case GATT_SUCCESS:
                gattResult = "GATT_SUCCESS";
                break;
            case BluetoothGatt.GATT_FAILURE:
                gattResult = "GATT_FAILURE";
                break;
            case BluetoothGatt.GATT_CONNECTION_CONGESTED:
                gattResult = "GATT_CONNECTION_CONGESTED";
                break;
            case BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION:
                gattResult = "GATT_INSUFFICIENT_AUTHENTICATION";
                break;
            case BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION:
                gattResult = "GATT_INSUFFICIENT_ENCRYPTION";
                break;
            case BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH:
                gattResult = "GATT_INVALID_ATTRIBUTE_LENGTH";
                break;
            case BluetoothGatt.GATT_INVALID_OFFSET:
                gattResult = "GATT_INVALID_OFFSET";
                break;
            case BluetoothGatt.GATT_READ_NOT_PERMITTED:
                gattResult = "GATT_READ_NOT_PERMITTED";
                break;
            case BluetoothGatt.GATT_WRITE_NOT_PERMITTED:
                gattResult = "GATT_INVALID_ATTRIBUTE_LENGTH";
                break;
            case BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED:
                gattResult = "GATT_REQUEST_NOT_SUPPORTED";
                break;
        }

        return gattResult;
    }
}
