package com.life.serializable.utils;

/**
 * Created by User on 14/02/2018.
 */

public interface ReaderThreadCallbacks {

    void onDataRead(byte[] data);

}
